﻿open System

let rec quicksort to_sort =
    match to_sort with
    | [] -> []
    | first::rest ->
        let less, greater =  List.partition (fun e -> e < first) to_sort
        List.concat [quicksort less; [first]; quicksort greater]

let square x = x * x

let square_sum n =
    [1..n] |> List.map square |> List.sum |> (*) 2


let rowSumOddNumbers n = 
    let first = 
        [1..(n-1)]
        |> List.map ((*) 2)
        |> List.sum
        |> (+) 1
    let summ =
        [first..2..(first+(n-1)*2)] |> List.sum
    summ
    
let longest (s1: string) (s2: string) =
    s1 + s2
    |> Seq.toList
    |> Seq.sort
    |> Seq.distinct
    |> Seq.toArray
    |> String

let mxdiflg(a1: String[]) (a2: String[]): int Option = 
    let a = Seq.toList a1
    let b = Seq.toList a2
    if a = [] || b = [] 
    then 
        None
    else
        let minmax s = (List.min s, List.max s)
        let mm l = 
            l |> List.map (fun s -> String.length s) |> minmax
        let (amin, amax) = mm a
        let (bmin, bmax) = mm b
        let res min  = 
            let r2 max = max - min
            r2
        let r1 = res amin bmax
        let r2 = res bmin amax
        Some (max r1 r2)

let rec my_map f data =
    match data with
    | [] -> []
    | first::tail ->
        [f first] @ my_map f tail

let rec my_for_each f data =
    match data with
    | [] -> ignore
    | first::tail ->
        f first
        my_for_each f tail

let rec my_reduce f init data =
    match data with
    | [] -> []
    | first::tail ->
        let new_init = f init first
        [new_init] @ my_reduce f new_init tail

let rec my_unzip data =
    my_reduce (@) [] data

let rec my_zip data1 data2 =
    match (data1, data2) with
    | (h1::tail1, h2::tail2) -> [[h1;h2]]@ my_zip tail1 tail2
    | (_, _) -> []


[<EntryPoint>]
let main args =
    let b = my_map square [1;2;3;4;]
    my_for_each (printf "%d") b

    0
