using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

/*
 * param1
 * param2
 * param3
 * condition1 param1 > param2 -> change param3
 * condition2 param3 < param2 -> change
 */

/*
 * oxygen, parametric equal concentration in volume
 * food = plants, produces oxygen when 
 *
 *
 */

/*
 * Cell cycle:
 *
 * breath (very often)
 * eat (sometimes)
 * make another cell (rarely)
 * make smell/blood when ready to breed or injured
 * 
 */


/*
 * input triggers
 *      external (collision with - bumb into something)
 *      internal (low health - hunger< lack of oxygen)
 * 
 * ? output triggers
 *      - signal to System to create something
 *      new (cell, smell, blood etc)     
 */

/*
 * variant 1
 * MainBehavior -> all types of objects
 *
 * variant 2
 * B
 * void in_trigg
 *  
 *      behavior 1
 *      behavior 2
 *      behavior 3
 *      
 *      
 * variant 3
 * 
 * Beh object - object with param-condition explanation
 *      beh_obj 1
 *      beh_obj 2
 *      beh_obj 3
 * 
 * Common Object type - type with system of transition of trigers
 * void in_trigg
 * void out_trigg
 * life cycle       <- beh_obj
 * 
 * 
 */



public class CellBehaviour : MonoBehaviour
{
    public MainScript Main;
    public GameObject CellPrefab;
    public float interval;


    // Start is called before the first frame update
    private void Awake()
    {
        Invoke(nameof(InstNewCell), interval);
    }

    private void InstNewCell()
    {
        var parent = transform.parent;
        Vector2 position = Random.onUnitSphere * 5f;
        var go = Instantiate(CellPrefab, transform.position + (Vector3)position, Quaternion.identity);
        go.GetComponent<Rigidbody2D>().velocity += position * 5f;
        go.transform.SetParent(parent);
    }

    // Update is called once per frame
    private void Update()
    {
        
    }
}
