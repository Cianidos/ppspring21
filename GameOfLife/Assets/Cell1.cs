using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class Cell1 : MonoBehaviour
{
    public GameObject CameraGameObject;

    public int InitCount;

    public GameObject Cell1Prefab;

    public Text fps;

    private Random _rand;
    private bool[,] _cells, _cells2;
    private int _leftW, _rightW, _upW, _downW;
    private Dictionary<Tuple<int,int> ,GameObject> _pool;

    private Vector3 CVector3(int x, int y)
    {
        return new Vector3(x, y, 0) - new Vector3(0.5f, 0.5f, 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        var cam = CameraGameObject.GetComponent<Camera>();
        _upW = (int) cam.orthographicSize;
        _downW = 0;
        _rightW = (int) (cam.aspect * _upW);
        _leftW = 0;
        CameraGameObject.transform.position += new Vector3(_rightW, _upW);
        _upW *= 2;
        _rightW *= 2;

        _rand = new System.Random(Time.captureFramerate);
        _cells = new bool[_rightW, _upW];
        _pool = new Dictionary<Tuple<int, int>, GameObject>();


        for (int i = 0; i < InitCount; ++i)
        {
            var x = _rand.Next(_leftW, _rightW);
            var y = _rand.Next(_downW, _upW);
            _cells[x, y] = true;
        }

        StartCoroutine(DoThis());

    }

    private int CircleX(int x)
    {
        if (x < 0) x += _rightW;
        if (x >= _rightW) x -= _rightW;
        return x;
    }
    private int CircleY(int y)
    {
        if (y < 0) y += _upW;
        if (y >= _upW) y -= _upW;
        return y;
    }

    private int CountNeighbors(int x, int y)
    {
        int res = 0;
        for (int i = -1; i < 2; ++i)
        {
            for (int j = -1; j < 2; ++j)
            {
                if (!(i == 0 && j == 0))
                {
                    res += (_cells[CircleX(x + i), CircleY(y + j)])? 1 : 0;
                }
            }
        }
        return res;
    }

    void FixedUpdate()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator DoThis()
    {
        while (true)
        {
            fps.text = ((int) (1f / Time.unscaledDeltaTime)).ToString();
            _cells2 = new bool[_rightW, _upW];

            for (int x = _leftW; x < _rightW; ++x)
            {
                for (int y = _downW; y < _upW; ++y)
                {
                    int neighbors = CountNeighbors(x, y);

                    if (!_cells[x, y])
                    {
                        if (neighbors == 3)
                            _cells2[x, y] = true;
                        else
                            _cells2[x, y] = false;
                    }
                    else
                    {
                        if (neighbors == 2 || neighbors == 3)
                            _cells2[x, y] = true;
                        else
                            _cells2[x, y] = false;
                    }
                }
            }

            _cells = _cells2;

            for (int x = _leftW; x < _rightW; ++x)
            {
                for (int y = _downW; y < _upW; ++y)
                {
                    var t = new Tuple<int, int>(x, y);
                    if (_cells[x, y])
                    {
                        if (!_pool.ContainsKey(t))
                            _pool.Add(t, Instantiate(Cell1Prefab, CVector3(x, y), Quaternion.identity));
                    }
                    else
                    {
                        if (_pool.ContainsKey(t))
                        {
                            Destroy(_pool[t]);
                            _pool.Remove(t);
                        }
                    }
                }
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
