﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = System.Random;

public class Init : MonoBehaviour
{
    public GameObject Obj;
    private Random _rand;

    // Start is called before the first frame update
    private Vector3 RandVector3()
    {
        return new Vector3((float) (_rand.NextDouble() * 16 - 8), (float) (_rand.NextDouble() * 16 - 8), 0);
    }

    void Start()
    {
        _rand = new Random(Time.frameCount);

        for (var i = 0; i < 100; ++i)
            Instantiate(Obj, RandVector3(), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
