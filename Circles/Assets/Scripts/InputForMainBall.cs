﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public class InputForMainBall : MonoBehaviour
{
    public GameObject Ball;
    private Gyroscope _gyro;

    // Start is called before the first frame update
    void Start()
    {
        _gyro = Input.gyro;
        _gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        var vertical = Input.GetAxis("Vertical");
        var horizontal = Input.GetAxis("Horizontal");

        var body = Ball.GetComponent<Rigidbody2D>();
        body.AddForce(new Vector2(horizontal, vertical));

        vertical = _gyro.gravity.y;
        horizontal = _gyro.gravity.x;

        body.AddForce(new Vector2(horizontal * 9.0f, vertical * 9.0f));
    }
}
